FROM gcr.io/kaniko-project/executor:debug

ENTRYPOINT []
SHELL ["/busybox/sh", "-c"]

RUN mkdir -p /usr/local/bin
RUN ln -s /busybox/sh /usr/local/bin/sh